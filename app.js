var ws = require('ws').WebSocketServer;
var uid = require('uid');
var URL = require('url');
var PATH = require('path');
var QUERY = require('querystring');
var _ = require('lodash');
var http = require('http');

var server = http.createServer(function (req, res) {
    var url = URL.parse(req.url);
    console.log(url);
    if(url.path === '/crossdomain.xml'){
        res.writeHead(200, {'Content-Type': 'application/xml'});
        res.end('<?xml version="1.0"?><cross-domain-policy><allow-access-from domain="*" to-ports="8080-1220"/></cross-domain-policy>');
    }else{
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.end('{"route":"default"}');
    }


});

var UserConnectionArray = {};

server.listen(8080);

var WebSocketServer = require('ws').Server
    , wss = new WebSocketServer({ server: server });

function messageWrap(data){
    var action = Object.keys(data)[0];
    return JSON.stringify({data:data,action:action});
}

function broadcast(gameID, data, except){
    Object.keys(UserConnectionArray).forEach(function(userID){
        var connWrap = UserConnectionArray[userID];
        if(gameID === connWrap.gameID && userID !== except){
            connWrap.conn.send(messageWrap(data));
        }
    });
}

var GROUP_MAX = 4;

function getNextGameID(){
    console.log("Current USERS",Object.keys(UserConnectionArray));
    var characters = _.groupBy(UserConnectionArray, function(connWrapper) { return connWrapper.gameID; });
    var hasOpening = _.find(characters, function(chr) {
        return chr.length < GROUP_MAX;
    });
    if(hasOpening && hasOpening.length){
        console.log("EXISTING GAME",hasOpening[0].gameID);
        return {id:hasOpening[0].gameID,newGame:false,start:hasOpening[0].start};
    }
    var _uid = uid(10);
    console.log("NEW GAME",_uid);
    return {id:_uid,newGame:true};
}

function getUsersInGame(gameID){
    Object.keys(UserConnectionArray).filter(function(userID){
        return UserConnectionArray[userID].gameID === gameID;
    }).map(function(userID){
        return UserConnectionArray[userID].user;
    });
}

wss.on('connection', function connection(conn) {

    var userID = uid(10);

    var url = URL.parse(conn.upgradeReq.url);
    var connectionWrapper = {
        conn:conn
    };

    var gameData = getNextGameID();
    var isNewGame = gameData.newGame;
    var gameID = connectionWrapper.gameID = gameData.id;
    var userData = connectionWrapper.user = QUERY.parse(url.query);
    var gameStart = connectionWrapper.start = (isNewGame?userData.start:gameData.start);


    conn.on('message', function incoming(message) {
        var parsed = JSON.parse(message);
        console.log("Incoming",parsed,gameID);
        if(parsed.jump){
            // may need unique handlers
            return broadcast(gameID,parsed);
        }
        if(parsed.win || parsed.die){
            // may need unique handlers

            Object.keys(UserConnectionArray).forEach(function(userID){
                var connWrap = UserConnectionArray[userID];
                if(gameID === connWrap.gameID){
                    console.log("Kill game");
                    delete UserConnectionArray[userID];
                }
            });
        }
    });

    conn.on('close', function close() {
        delete UserConnectionArray[userID];
        console.log("DISC", userData,gameID);
        if(userData && Object.keys(userData).length){
            broadcast(gameID,{goodbye:userData});
        }
    });

    conn.on('error', function err() {
        delete UserConnectionArray[userID];
        console.log("ERR", userData,gameID);
        if(userData && Object.keys(userData).length){
            broadcast(gameID,{goodbye:userData});
        }
    });

    UserConnectionArray[userID] = connectionWrapper;

    conn.send(messageWrap({hello:{userID:userID,game:gameID,username:userData.username,newGame:isNewGame,start:gameStart}}));
    console.log("CONN", userData,gameID);
    broadcast(gameID,{join:{userID:userID,game:gameID,username:userData.username},current:getUsersInGame(gameID)},userID);
});